import 'package:flutter/material.dart';

class EmployeeList extends StatelessWidget {
  const EmployeeList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.black54,
          ),
          backgroundColor: Colors.white,
          title: Text(
            'Employee list',
            style: TextStyle(color: Colors.black),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Icon(
                Icons.search,
                color: Colors.black54,
              ),
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [ListItem()],
          ),
        ),
      ),
    );
  }
}

class ListItem extends StatelessWidget {
  const ListItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 15),
      child: Row(
        children: [
          Container(
            height: 120,
            width: 120,
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
            child: Image.asset(
              'assets/images/profile_pic.jpg',
              height: 120,
              width: 120,
            ),
          ),
          Container(
            height: 90,
            width: 220,
            // color: Colors.black,
            decoration: new BoxDecoration(
              color: Colors.amber,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(20),
                bottomRight: Radius.circular(20),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
