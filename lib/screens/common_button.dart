import 'package:flutter/material.dart';
import 'package:login_page/screens/employee_list.dart';

class AllButtons extends StatelessWidget {
  final double? height;
  final double? width;
  final String? fontfamily;
  final double? size;
  final Color? decorationColor;
  final Color? textColor;
  final BorderRadius? borderRadius;
  final FontWeight? fontWeight;
  final FontStyle? fontStyle;
  final Color borderColor = Colors.blue;
  final String? lableText;

  AllButtons(
      {this.height,
      this.width,
      this.borderRadius,
      this.decorationColor,
      this.fontStyle,
      this.fontWeight,
      this.fontfamily,
      this.size,
      this.lableText,
      this.textColor});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.5, left: 10, right: 10),
      child: Container(
        height: height,
        width: width,
        decoration:
            BoxDecoration(color: decorationColor, borderRadius: borderRadius),
        child: Center(
          child: Text(
            lableText!,
            style: TextStyle(
              fontFamily: fontfamily,
              color: textColor,
              fontSize: size,
              fontWeight: fontWeight,
              fontStyle: fontStyle,
            ),
          ),
        ),
      ),
    );
  }
}

class SignInWithGoogle extends StatefulWidget {
  @override
  _SignInWithGoogleState createState() => _SignInWithGoogleState();
}

class _SignInWithGoogleState extends State<SignInWithGoogle> {
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
      child: InkWell(
        onTap: () {
          setState(() {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => EmployeeList()),
            );
          });
        },
        child: Container(
          height: 50,
          width: 370,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.blue, width: 2),
              borderRadius: BorderRadius.circular(27)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/google_icon.png',
                height: 30,
              ),
              SizedBox(width: 10),
              Text(
                'Sign in with Google',
                style: TextStyle(
                  fontFamily: 'SegoeUI',
                  color: Colors.black,
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
